# Blog Structure: www.picolisp-explored.com

## About

1. [Functional Programming in PicoLisp](https://picolisp-explored.com/functional-programming-in-picolisp)
2. [What is Functional Programming?](https://picolisp-explored.com/what-is-functional-programming)
3. [Why PicoLisp is great for Functional Programming](https://picolisp-explored.com/why-picolisp-is-great-for-functional-programming)
4. [Why learn PicoLisp? How to Read this Blog](https://picolisp-explored.com/why-learn-picolisp-how-to-read-this-blog)
5. [PicoLisp Plans for 2022](https://picolisp-explored.com/picolisp-plans-for-2022)

## Series: Getting Started with PicoLisp

1. [How to install PicoLisp](https://picolisp-explored.com/how-to-install-picolisp) 
2. [Pros and Cons of PicoLisp Unfolded](https://picolisp-explored.com/pros-and-cons-of-picolisp-unfolded) 
3. [Concepts and data types of PicoLisp](https://picolisp-explored.com/concepts-and-data-types)
4. [60 PicoLisp Functions You Should Know - 1: Arithmetics](https://picolisp-explored.com/60-picolisp-functions-you-should-know-1-arithmetics)
5. [60 PicoLisp Functions You Should Know - 2: Defining Variables](https://picolisp-explored.com/60-picolisp-functions-you-should-know-2-defining-variables)
6. [60 PicoLisp Functions You Should Know - 3: Input/Output](https://picolisp-explored.com/60-picolisp-functions-you-should-know-3-inputoutput)
7. [60 PicoLisp Functions You Should Know - 4: Control Flow](https://picolisp-explored.com/60-picolisp-functions-you-should-know-part-4-control-flow)
8. [60 PicoLisp Functions You Should Know - 5: Defining Own Functions](https://picolisp-explored.com/60-picolisp-functions-you-should-know-5-defining-own-functions)
9. [60 PicoLisp Functions You Should Know - 6: List and Strings](https://picolisp-explored.com/60-picolisp-functions-you-should-know-6-lists-and-strings)
10. [Floating Point vs. Fixed Point Arithmetics](https://picolisp-explored.com/floating-point-vs-fixed-point-arithmetics)
11. [A very first PicoLispProgram](https://picolisp-explored.com/a-very-first-picolisp-program)
12. [Levelling up - 1: Read the docs](https://picolisp-explored.com/levelling-up-1-read-the-docs)
13. [Levelling up - 2: Use the debugger](https://picolisp-explored.com/levelling-up-2-use-the-debugger)


## Web Applications Tutorials

1. [Web Application Programming in PicoLisp](https://picolisp-explored.com/web-application-programming-in-picolisp)
2. [How it works - the HTML function](https://picolisp-explored.com/web-application-programming-in-picolisp-how-it-works)
3. [Setting up the server](https://picolisp-explored.com/web-application-programming-in-picolisp-setting-up-the-server)
4. [Adding HTML Tags](https://picolisp-explored.com/web-application-programming-in-picolisp-adding-html-tags)
5. [Adding CSS](https://picolisp-explored.com/web-application-programming-in-picolisp-adding-css)
6. [Creating HTML Forms, Part 1](https://picolisp-explored.com/web-application-programming-in-picolisp-creating-html-forms-part-1)
7. [Creating HTML Forms, Part 2](https://picolisp-explored.com/web-application-programming-in-picolisp-creating-html-forms-part-2)
8. [Introducing the GUI Framework](https://picolisp-explored.com/web-application-programming-in-picolisp-introducing-the-gui-framework)
9. [Prefix Classes](https://picolisp-explored.com/web-application-programming-in-picolisp-prefix-classes)
10. [Some more GUI elements](https://picolisp-explored.com/web-application-programming-in-picolisp-some-more-gui-elements)
11. [How to create a To-Do App in PicoLisp (Desktop Version)](https://picolisp-explored.com/how-to-create-a-to-do-app-in-picolisp-desktop-version)
12. [How to create a To-Do App in PicoLisp (Responsive Version) - Part 1](https://picolisp-explored.com/how-to-create-a-to-do-app-in-picolisp-responsive-version-part-1)
13. [How to create a To-Do App in PicoLisp (Responsive Version) - Part 2](https://picolisp-explored.com/how-to-create-a-to-do-app-in-picolisp-responsive-version-part-2)
14. [Creating a Todo App - 3: Adding the Database](https://picolisp-explored.com/creating-a-todo-app-3-adding-the-database)
15. [Creating a Todo App - 4: Customizing the Database Output](https://picolisp-explored.com/creating-a-todo-app-4-customizing-the-database-output)
16. [User Administration with the adm.l library](https://picolisp-explored.com/user-administration-with-the-adml-library)
17. [Creating a Todo App - 5: Adding the User Login](https://picolisp-explored.com/creating-a-todo-app-5-adding-the-user-login)
18. [Creating a Todo App - 6: Multi-Language Support](https://picolisp-explored.com/creating-a-todo-app-6-multi-language-support)
19. [Creating a Todo App - 7: Database Manipulation via API](https://picolisp-explored.com/creating-a-todo-app-7-database-manipulation-via-api)
20. [Creating a Todo App - 8: Going into Production with the httpGate Proxy Server](https://picolisp-explored.com/creating-a-todo-app-8-going-into-production-with-the-httpgate-proxy-server)
21. [Web App Example: A Simple Address Book](https://picolisp-explored.com/web-app-example-a-simple-address-book)
22. [Web App Example: A full-stack ERP App](https://picolisp-explored.com/web-app-example-a-full-stack-erp-app)
23. [The PicoLisp Canvas Library](https://picolisp-explored.com/the-picolisp-canvas-library)
24. [Creating dynamic graphs with Canvas](https://picolisp-explored.com/creating-dynamic-graphs-with-canvas)


## Mobile App Development

1. [Mobile App Development in PicoLisp - I: Android Basics](https://picolisp-explored.com/mobile-app-development-in-picolisp-i-android-basics)
2. [Mobile App Development in PicoLisp - II: How to install the "PilBox"](https://picolisp-explored.com/mobile-app-development-in-picolisp-ii-how-to-install-the-pilbox)
3. [Mobile App Development in PicoLisp - III: Installing Demo Applications](https://picolisp-explored.com/mobile-app-development-in-picolisp-iii-installing-demo-applications)
4. [Mobile App Development in PicoLisp - IV: Using the PicoLisp REPL](https://picolisp-explored.com/mobile-app-development-in-picolisp-iv-using-the-pilbox-repl)
5. [Mobile App Development in PicoLisp - V: Getting a Remote Shell to your PC](https://picolisp-explored.com/mobile-app-development-in-picolisp-v-getting-a-remote-shell-to-your-pc)
6. [Mobile App Development in PicoLisp - VI: Intents and the Java Interface](https://picolisp-explored.com/mobile-app-development-in-picolisp-vi-intents-and-the-java-interface)
7. [Mobile App Development in PicoLisp - VII: Basic App Components](https://picolisp-explored.com/mobile-app-development-in-picolisp-vii-basic-app-components)
8. [A Camera Android App Written in PicoLisp (Pt. 1)](https://picolisp-explored.com/a-camera-android-app-written-in-picolisp-pt-1)
9. [A Camera Android App Written in PicoLisp (Pt. 2)](https://picolisp-explored.com/a-camera-android-app-written-in-picolisp-pt-2)
10. [An OpenStreetMap App Written in PicoLisp (Pt. 1)](https://picolisp-explored.com/an-openstreetmap-app-written-in-picolisp-pt-1)
11. [An OpenStreetMap App Written in PicoLisp (Pt. 2)](https://picolisp-explored.com/an-openstreetmap-app-written-in-picolisp-pt-2)
12. [An OpenStreetMap App Written in PicoLisp (Pt. 3)](https://picolisp-explored.com/an-openstreetmap-app-written-in-picolisp-pt-3)
13. [Improving User Experience with Server-Side Events](https://picolisp-explored.com/improving-user-experience-with-server-sent-events)


## The PicoLisp Database

1. [Introduction to the PicoLisp Database](https://picolisp-explored.com/introduction-to-the-picolisp-database)
2. [Getting Started with the PicoLisp Database](https://picolisp-explored.com/getting-started-with-the-picolisp-database)
3. [How to Define Entities and Relationships in the PicoLisp Database](https://picolisp-explored.com/how-to-define-entities-and-relationships-in-the-picolisp-database)
4. [How to Add Records to the PicoLisp Database](https://picolisp-explored.com/how-to-add-records-to-the-picolisp-database)
5. [PicoLisp Explored: Working with multiple database files](https://picolisp-explored.com/picolisp-explored-working-with-multiple-database-files)
6. [Creating a User Interface to the Database: Setup](https://picolisp-explored.com/creating-a-user-interface-to-the-database-setup)
7. [Creating A User Interface to the Database: Displaying the Data](https://picolisp-explored.com/creating-a-user-interface-to-the-database-displaying-the-data)
8. [How to create a RESTful API to the PicoLisp](https://picolisp-explored.com/how-to-create-a-restful-api-to-the-picolisp-database)
9. [Creating a User Interface for Data Modification, Part 1](https://picolisp-explored.com/creating-a-user-interface-for-data-modification-part-1)
10. [Creating a User Interface for Data Modification, Part 2](https://picolisp-explored.com/creating-a-user-interface-for-data-modification-part-2)
11. [How to hack into the PicoLisp Database (and how to prevent it!)](https://picolisp-explored.com/how-to-hack-into-the-picolisp-database-and-how-to-prevent-it)
12. [How to write database queries in Pilog](https://picolisp-explored.com/how-to-write-database-queries-with-pilog)
13. ["Poor Man's SQL": Accessing the PicoLisp Database with SELECT](https://picolisp-explored.com/poor-mans-sql-accessing-the-picolisp-database-with-select)
14. [Handling Complex Database Queries](https://picolisp-explored.com/handling-complex-database-queries)
15. [Creating PDF Output](https://picolisp-explored.com/creating-pdf-output)


## Introduction to Pilog

1. [Logical Programming in PicoLisp: Learning Pilog!](https://picolisp-explored.com/logical-programming-in-picolisp-learning-pilog)
2. [Learning Pilog - 1: A Short Introduction to Prolog](https://picolisp-explored.com/learning-pilog-1-a-short-introduction-to-prolog)
3. [Learning Pilog - 2: Facts, Rules, Queries](https://picolisp-explored.com/learning-pilog-2-facts-rules-queries)
4. [Learning Pilog - 3: Unification and Proof Search](https://picolisp-explored.com/learning-pilog-3-unification-and-proof-search)
5. [Learning Pilog - 4: Recursion](https://picolisp-explored.com/learning-pilog-4-recursion)
6. [Learning Pilog - 5: Lists](https://picolisp-explored.com/learning-pilog-5-lists)
7. [Learning Pilog - 6: More Lists](https://picolisp-explored.com/learning-pilog-6-more-lists)
8. [Learning Pilog - 7: Cuts and Negations](https://picolisp-explored.com/learning-pilog-7-cuts-and-negations)
9. [How to use Pilog in PicoLisp](https://picolisp-explored.com/how-to-use-pilog-in-picolisp)


## PicoLisp Explored

1. [The set function](https://picolisp-explored.com/picolisp-explored-the-set-function)
2. [Piping from the Command Line](https://picolisp-explored.com/picolisp-explored-piping-from-the-command-line)
3. [Binary Tree - the idx function](https://picolisp-explored.com/picolisp-explored-the-idx-function)
4. [The FIFO function](https://picolisp-explored.com/picolisp-explored-the-fifo-function)
5. [Binary Tree - the cache function](https://picolisp-explored.com/picolisp-explored-the-cache-function)
6. [Binary Tree - the enum function](https://picolisp-explored.com/picolisp-explored-the-enum-function)
7. [Object-Oriented Programming, Part 1](https://picolisp-explored.com/picolisp-explored-object-oriented-programming-part-1)
8. [Object-Oriented Programming, Part 2](https://picolisp-explored.com/picolisp-explored-object-oriented-programming-part-2)
9. [How to Add History to the REPL Config File](https://picolisp-explored.com/how-to-add-history-to-the-repl-config-file)
10. [Introduction to Vip - An Editor for PicoLisp Development](https://picolisp-explored.com/introduction-to-vip-an-editor-for-picolisp-development)
11. [Visualizing Data Structures with Vip](https://picolisp-explored.com/visualizing-data-structures-with-vip)
12. [Developing and Debugging with Vip](https://picolisp-explored.com/developing-and-debugging-with-vip)
13. [The curry function](https://picolisp-explored.com/picolisp-explored-the-curry-function)
14. [Working with lists - push, pop, and more.](https://picolisp-explored.com/working-with-lists-push-pop-and-more)
15. [Working with lists - mapping functions](https://picolisp-explored.com/working-with-lists-mapping-functions)
16. [Working with lists - destructive operations](https://picolisp-explored.com/working-with-lists-destructive-operations)
17. [PicoLisp Explored - The Task function](https://picolisp-explored.com/picolisp-explored-the-task-function)
18. [PicoLisp Explored - Writing your own little Chat App](https://picolisp-explored.com/picolisp-explored-writing-your-own-little-chat-app)


## The Project Euler 

1. [About the Euler Project](https://picolisp-explored.com/about-the-euler-project)
2. [Multiples of 3 and 5](https://picolisp-explored.com/riddle-1-multiples-of-3-or-5)
3. [Fibonacci - even Fibonacci numbers](https://picolisp-explored.com/riddle-2-even-fibonacci-numbers)
3. [Riddle 3: Largest Prime Factor](https://picolisp-explored.com/riddle-3-largest-prime-factor)
4. [Riddle 4: Largest Palindrome Product](https://picolisp-explored.com/riddle-4-largest-palindrome-product)
5. [Riddle 5: Smallest Multiple](https://picolisp-explored.com/riddle-5-smallest-multiple)
6. [Riddle 6: Sum Square Difference](https://picolisp-explored.com/riddle-6-sum-square-difference)
7. [Riddle 7: 10001st Prime](https://picolisp-explored.com/riddle-7-10001st-prime)
8. [Riddle 8: Largest Product in a Series](https://picolisp-explored.com/riddle-8-largest-product-in-a-series)
9. [Riddle 9: Special Pythagorean Triplet](https://picolisp-explored.com/riddle-9-special-pythagorean-triplet)
10. [Riddle 10: Summation of Primes](https://picolisp-explored.com/riddle-10-summation-of-primes)


## The Rosetta Code Project

1. [Abou the Rosetta Code Project](https://picolisp-explored.com/the-rosetta-code-project)
2. [Caesar cipher](https://picolisp-explored.com/caesar-cipher-a-simple-encryption-system-in-picolisp)
3. [100 Doors](https://picolisp-explored.com/100-doors)
4. [Binary Tree Traversal, Part 1](https://picolisp-explored.com/binary-tree-traversal-part-1)
5. [Binary Tree Traversal, Part 2](https://picolisp-explored.com/binary-tree-traversal-part-2)
6. [The Fibonacci Sequence](https://picolisp-explored.com/the-fibonacci-sequence)
7. [Object-Oriented Programming Examples](https://picolisp-explored.com/rosetta-code-object-oriented-programming-examples)
8. [Web Apps: A Simple Windowed Application](https://picolisp-explored.com/webapps-a-simple-windowed-application)
9. [Web Apps: GUI Component Interaction](https://picolisp-explored.com/web-apps-gui-component-interaction)
10. [https://picolisp-explored.com/web-apps-gui-enablingdisabling-of-controls](https://picolisp-explored.com/web-apps-gui-enablingdisabling-of-controls)
11. [https://picolisp-explored.com/web-apps-an-animation](https://picolisp-explored.com/web-apps-an-animation)
12. [The Towers of Hanoi](https://picolisp-explored.com/the-towers-of-hanoi)
13. [Who owns the Zebra? A logic Puzzle](https://picolisp-explored.com/who-owns-the-zebra-a-logic-puzzle)
14. [Dinesman's Multiple Dwelling Problem](https://picolisp-explored.com/dinesmans-multiple-dwelling-problem)
15. [Optimal Route Planning: A Brute Force Solution](https://picolisp-explored.com/optimal-route-planning-a-brute-force-solution)
16. [The Forest Fire Model: Let it burn!](https://picolisp-explored.com/the-forest-fire-model-let-it-burn)


## Community Activity

1. [How to join the community](https://picolisp-explored.com/how-to-join-the-picolisp-community)
2. [The PicoLisp Wiki and other Resources](https://picolisp-explored.com/the-picolisp-wiki-and-other-resources)

